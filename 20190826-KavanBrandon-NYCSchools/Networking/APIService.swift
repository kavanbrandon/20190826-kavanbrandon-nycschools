//
//  APIService.swift
//  20190826-KavanBrandon-NYCSchools
//
//  Created by Kavan Brandon on 8/26/19.
//  Copyright © 2019 KavanBrandon. All rights reserved.
//
import Foundation

class APIService {
    
    static let shared = APIService()
    
    //Offset used for pagination of results.
    func searchSchools(searchTerm: String, offset: Int, completion: @escaping ([School], Error?) -> ()) {
        let urlString = "https://data.cityofnewyork.us/resource/s3k6-pzi2.json?$limit=20&$offset=\(offset)&$order=:id&$where=school_name%20like%20%27%25\(searchTerm)%25%27"
        guard let url = URL(string: urlString) else { return }
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            if let error = error {
                print(error)
                completion([], nil)
                return
            }
            
            guard let data = data else { return }

            do {
                let searchResult = try JSONDecoder().decode([School].self, from: data)
                completion(searchResult, nil)

            } catch let error {
                print(error)
                completion([], error)
            }
            }.resume()
    }
    
    //Offset used for pagination of results.
    func fetchSchools(offset: Int, completion: @escaping ([School], Error?) -> ()) {
        let urlString = "https://data.cityofnewyork.us/resource/s3k6-pzi2.json?$limit=20&$offset=\(offset)"

        guard let url = URL(string: urlString) else { return }
        
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            if let error = error {
                print(error)
                completion([], nil)
                return
            }
            
            guard let data = data else { return }
            
            do {
                let searchResult = try JSONDecoder().decode([School].self, from: data)
                completion(searchResult, nil)
                
            } catch let error {
                print(error)
                completion([], error)
            }
            }.resume()
    }
    
    func fetchSchool(dbn: String, completion: @escaping ([School], Error?) -> ()) {
        let urlString = "https://data.cityofnewyork.us/resource/s3k6-pzi2.json?dbn=\(dbn)"

        guard let url = URL(string: urlString) else { return }
        
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            if let error = error {
                print(error)
                completion([], nil)
                return
            }
            
            guard let data = data else { return }
            
            do {
                let school = try JSONDecoder().decode([School].self, from: data)
                completion(school, nil)
                
            } catch let error {
                print(error)
                completion([], error)
            }
            }.resume()
    }
    
    func fetchSATResult(dbn: String, completion: @escaping ([Result], Error?) -> ()) {
        let urlString = "https://data.cityofnewyork.us/resource/f9bf-2cp4.json?dbn=\(dbn)"
        
        guard let url = URL(string: urlString) else { return }
        
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            if let error = error {
                print(error)
                completion([], nil)
                return
            }
            
            guard let data = data else { return }
            
            do {
                let school = try JSONDecoder().decode([Result].self, from: data)
                completion(school, nil)
                
            } catch let error {
                print(error)
                completion([], error)
            }
            }.resume()
    }
}
