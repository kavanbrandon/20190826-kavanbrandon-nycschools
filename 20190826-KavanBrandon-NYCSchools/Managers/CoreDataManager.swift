//
//  CoreDataManager.swift
//  20190826-KavanBrandon-NYCSchools
//
//  Created by Kavan Brandon on 8/27/19.
//  Copyright © 2019 KavanBrandon. All rights reserved.
//

import Foundation
import CoreData
import UIKit

class CoreDataManager {
    
    let persistentContainer: NSPersistentContainer!
    
    lazy var backgroundContext: NSManagedObjectContext = {
        return self.persistentContainer.newBackgroundContext()
    }()
    
    init(container: NSPersistentContainer) {
        self.persistentContainer = container
        self.persistentContainer.viewContext.automaticallyMergesChangesFromParent = true
    }
        
    convenience init() {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            fatalError("Can not get shared app delegate")
        }
        self.init(container: appDelegate.persistentContainer)
    }
    
    //Used to save school to the core data stack.
    func persistSchoolToFavorites(school: School) {
        let managedContext = persistentContainer.viewContext
        
        let entity = NSEntityDescription.entity(forEntityName: "School", in: managedContext)!
        
        let schoolResult = NSManagedObject(entity: entity, insertInto: managedContext)
        
        schoolResult.setValue(school.dbn, forKeyPath: "dbn")
        schoolResult.setValue(school.schoolName, forKeyPath: "schoolName")
        schoolResult.setValue(school.overviewParagraph, forKeyPath: "overviewParagraph")
        schoolResult.setValue(school.neighborhood, forKeyPath: "neighborhood")
        schoolResult.setValue(school.location, forKeyPath: "location")
        schoolResult.setValue(school.phoneNumber, forKeyPath: "phoneNumber")
        schoolResult.setValue(school.schoolEmail, forKeyPath: "schoolEmail")
        schoolResult.setValue(school.website, forKeyPath: "website")
        schoolResult.setValue(school.subway, forKeyPath: "subway")
        schoolResult.setValue(school.bus, forKeyPath: "bus")
        schoolResult.setValue(school.totalStudents, forKeyPath: "totalStudents")
        schoolResult.setValue(school.extracurricularActivities, forKeyPath: "extracurricularActivities")
        schoolResult.setValue(school.schoolSports, forKeyPath: "schoolSports")
        schoolResult.setValue(school.attendanceRate, forKeyPath: "attendanceRate")
        schoolResult.setValue(school.directions1, forKeyPath: "directions1")
        schoolResult.setValue(school.primaryAddressLine1, forKeyPath: "primaryAddressLine1")
        schoolResult.setValue(school.city, forKeyPath: "city")
        schoolResult.setValue(school.zip, forKeyPath: "zip")
        schoolResult.setValue(school.latitude, forKeyPath: "latitude")
        schoolResult.setValue(school.longitude, forKeyPath: "longitude")
        schoolResult.setValue(school.borough, forKeyPath: "borough")
        
        do {
            try managedContext.save()
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
    }
    
    //Used to remove school from the core data stack.
    func removeSchoolFromFavorites(dbn: String) {
        let managedContext = persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "School")
        fetchRequest.predicate = NSPredicate(format: "dbn = %@", dbn)
        
        var results: [NSManagedObject] = []
        
        do {
            results = try managedContext.fetch(fetchRequest)
        }
        catch {
            print("error executing fetch request: \(error)")
        }
        
        if let result = results.first {
            do {
                managedContext.delete(result)
                try managedContext.save()
            } catch {
                print("error executing fetch request: \(error)")
            }
        }
    }
    
    //Used to check if school exists within core data
    func checkIfFavoriteExists(dbn: String) -> Bool {
        let managedContext = persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "School")
        fetchRequest.predicate = NSPredicate(format: "dbn = %@", dbn)
        
        var results: [NSManagedObject] = []
        
        do {
            results = try managedContext.fetch(fetchRequest)
        }
        catch {
            print("error executing fetch request: \(error)")
        }
        
        return results.count > 0
    }
    
    //Fetches all favorites for use by view controllers
    func fetchAllFavorites() -> [NSManagedObject] {
        let managedContext = persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "School")
        var favoritedSchools = [NSManagedObject]()
        
        do {
            favoritedSchools = try managedContext.fetch(fetchRequest)
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
        
        return favoritedSchools
    }
    
    func save() {
        if backgroundContext.hasChanges {
            do {
                try backgroundContext.save()
            } catch {
                print("Save error \(error)")
            }
        }
    }
}
