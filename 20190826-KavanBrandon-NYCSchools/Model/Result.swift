//
//  Result.swift
//  20190826-KavanBrandon-NYCSchools
//
//  Created by Kavan Brandon on 8/27/19.
//  Copyright © 2019 KavanBrandon. All rights reserved.
//

import Foundation

struct Result : Codable {
    let dbn : String?
    let schoolName : String?
    let numberOfTesters : String?
    let averageReadingScore : String?
    let averageMathScore : String?
    let averageWritingScore : String?
    
    enum CodingKeys: String, CodingKey {
        
        case dbn = "dbn"
        case schoolName = "school_name"
        case numberOfTesters = "num_of_sat_test_takers"
        case averageReadingScore = "sat_critical_reading_avg_score"
        case averageMathScore = "sat_math_avg_score"
        case averageWritingScore = "sat_writing_avg_score"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        dbn = try values.decodeIfPresent(String.self, forKey: .dbn)
        schoolName = try values.decodeIfPresent(String.self, forKey: .schoolName)
        numberOfTesters = try values.decodeIfPresent(String.self, forKey: .numberOfTesters)
        averageReadingScore = try values.decodeIfPresent(String.self, forKey: .averageReadingScore)
        averageMathScore = try values.decodeIfPresent(String.self, forKey: .averageMathScore)
        averageWritingScore = try values.decodeIfPresent(String.self, forKey: .averageWritingScore)
    }
}
