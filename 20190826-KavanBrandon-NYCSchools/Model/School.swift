//
//  Schools.swift
//  20190826-KavanBrandon-NYCSchools
//
//  Created by Kavan Brandon on 8/26/19.
//  Copyright © 2019 KavanBrandon. All rights reserved.
//

import Foundation

struct School : Codable {
    let dbn : String?
    let schoolName : String?
    let overviewParagraph : String?
    let neighborhood : String?
    let location : String?
    let phoneNumber : String?
    let schoolEmail : String?
    let website : String?
    let subway : String?
    let bus : String?
    let totalStudents : String?
    let extracurricularActivities : String?
    let schoolSports : String?
    let attendanceRate : String?
    let directions1 : String?
    let primaryAddressLine1 : String?
    let city : String?
    let zip : String?
    let latitude : String?
    let longitude : String?
    let borough : String?
    
    enum CodingKeys: String, CodingKey {
        
        case dbn = "dbn"
        case schoolName = "school_name"
        case overviewParagraph = "overview_paragraph"
        case neighborhood = "neighborhood"
        case location = "location"
        case phoneNumber = "phone_number"
        case schoolEmail = "school_email"
        case website = "website"
        case subway = "subway"
        case bus = "bus"
        case totalStudents = "total_students"
        case extracurricularActivities = "extracurricular_activities"
        case schoolSports = "school_sports"
        case attendanceRate = "attendance_rate"
        case directions1 = "directions1"
        case primaryAddressLine1 = "primary_address_line_1"
        case city = "city"
        case zip = "zip"
        case latitude = "latitude"
        case longitude = "longitude"
        case borough = "borough"
    }
    
    init(dbn: String, schoolName: String, overviewParagraph: String, neighborhood: String, location: String, phoneNumber: String, schoolEmail: String, website: String, subway: String, bus: String, totalStudents: String, extracurricularActivities: String, schoolSports: String, attendanceRate: String, directions1: String, primaryAddressLine1: String, city: String, zip: String, latitude: String, longitude: String, borough: String) {
        self.dbn = dbn
        self.schoolName = schoolName
        self.overviewParagraph = overviewParagraph
        self.neighborhood = neighborhood
        self.location = location
        self.phoneNumber = phoneNumber
        self.schoolEmail = schoolEmail
        self.website = website
        self.subway = subway
        self.bus = bus
        self.totalStudents = totalStudents
        self.extracurricularActivities = extracurricularActivities
        self.schoolSports = schoolSports
        self.attendanceRate = attendanceRate
        self.directions1 = directions1
        self.primaryAddressLine1 = primaryAddressLine1
        self.city = city
        self.zip = zip
        self.latitude = latitude
        self.longitude = longitude
        self.borough = borough
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        dbn = try values.decodeIfPresent(String.self, forKey: .dbn)
        schoolName = try values.decodeIfPresent(String.self, forKey: .schoolName)
        overviewParagraph = try values.decodeIfPresent(String.self, forKey: .overviewParagraph)
        neighborhood = try values.decodeIfPresent(String.self, forKey: .neighborhood)
        location = try values.decodeIfPresent(String.self, forKey: .location)
        phoneNumber = try values.decodeIfPresent(String.self, forKey: .phoneNumber)
        schoolEmail = try values.decodeIfPresent(String.self, forKey: .schoolEmail)
        website = try values.decodeIfPresent(String.self, forKey: .website)
        subway = try values.decodeIfPresent(String.self, forKey: .subway)
        bus = try values.decodeIfPresent(String.self, forKey: .bus)
        totalStudents = try values.decodeIfPresent(String.self, forKey: .totalStudents)
        extracurricularActivities = try values.decodeIfPresent(String.self, forKey: .extracurricularActivities)
        schoolSports = try values.decodeIfPresent(String.self, forKey: .schoolSports)
        attendanceRate = try values.decodeIfPresent(String.self, forKey: .attendanceRate)
        directions1 = try values.decodeIfPresent(String.self, forKey: .directions1)
        primaryAddressLine1 = try values.decodeIfPresent(String.self, forKey: .primaryAddressLine1)
        city = try values.decodeIfPresent(String.self, forKey: .city)
        zip = try values.decodeIfPresent(String.self, forKey: .zip)
        latitude = try values.decodeIfPresent(String.self, forKey: .latitude)
        longitude = try values.decodeIfPresent(String.self, forKey: .longitude)
        borough = try values.decodeIfPresent(String.self, forKey: .borough)
    }
}
