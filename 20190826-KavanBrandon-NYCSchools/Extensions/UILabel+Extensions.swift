//
//  UILabel+Extensions.swift
//  20190826-KavanBrandon-NYCSchools
//
//  Created by Kavan Brandon on 8/27/19.
//  Copyright © 2019 KavanBrandon. All rights reserved.
//
import UIKit

extension UILabel {
    convenience init(text: String, font: UIFont, numberOfLines: Int = 1) {
        self.init(frame: .zero)
        self.text = text
        self.font = font
        self.numberOfLines = numberOfLines
    }
}
