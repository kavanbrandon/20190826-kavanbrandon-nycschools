//
//  String+Extensions.swift
//  20190826-KavanBrandon-NYCSchools
//
//  Created by Kavan Brandon on 8/26/19.
//  Copyright © 2019 KavanBrandon. All rights reserved.
//

import Foundation

extension String {
    func replace(target: String, withString: String) -> String {
        return replacingOccurrences(of: target, with: withString, options: NSString.CompareOptions.literal, range: nil)
    }
    
    func capitalizingFirstLetter() -> String {
        return prefix(1).capitalized + dropFirst()
    }
    
    mutating func capitalizeFirstLetter() {
        self = self.capitalizingFirstLetter()
    }
}
