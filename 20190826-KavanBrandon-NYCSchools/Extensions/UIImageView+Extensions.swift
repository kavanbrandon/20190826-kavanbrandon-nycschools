//
//  UIImageView+Extensions.swift
//  20190826-KavanBrandon-NYCSchools
//
//  Created by Kavan Brandon on 8/27/19.
//  Copyright © 2019 KavanBrandon. All rights reserved.
//
import UIKit

extension UIImageView {
    convenience init(cornerRadius: CGFloat) {
        self.init(image: nil)
        self.layer.cornerRadius = cornerRadius
        self.clipsToBounds = true
        self.contentMode = .scaleAspectFill
    }
}
