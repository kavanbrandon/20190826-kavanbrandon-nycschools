//
//  SubtitleTableViewCell.swift
//  20190826-KavanBrandon-NYCSchools
//
//  Created by Kavan Brandon on 8/26/19.
//  Copyright © 2019 KavanBrandon. All rights reserved.
//

import UIKit

class SubtitleTableViewCell: UITableViewCell {
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: .subtitle, reuseIdentifier: reuseIdentifier)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
