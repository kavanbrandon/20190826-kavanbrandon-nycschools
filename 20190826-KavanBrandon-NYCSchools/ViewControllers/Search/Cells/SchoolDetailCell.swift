//
//  SchoolDetailCell.swift
//  20190826-KavanBrandon-NYCSchools
//
//  Created by Kavan Brandon on 8/27/19.
//  Copyright © 2019 KavanBrandon. All rights reserved.
//
import UIKit

class SchoolDetailCell: UICollectionViewCell {
    
    var school: School? {
        didSet {
            longDescriptionLabel.text = school?.overviewParagraph
            logoImageView.image = UIImage(named: "doe_logo")
        }
    }

    var result: Result? {
        didSet {
            mathScoreLabel.text = "Math: \(result?.averageMathScore ?? "")"
            readingScoreLabel.text = "Reading: \(result?.averageReadingScore ?? "")"
            writingScoreLabel.text = "Writing: \(result?.averageWritingScore ?? "")"
        }
    }
    
    var viewMapActionPressed: (() -> Void)?
    var favorited = false
    var persistSchoolHandler: ((School) -> Void)?
    var removeFavoriteHandler: ((School) -> Void)?
    
    let logoImageView: UIImageView = {
        let imageView = UIImageView(cornerRadius: 16)
        imageView.backgroundColor = .white
        imageView.constrainWidth(constant: 140)
        imageView.constrainHeight(constant: 140)
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    let mathScoreLabel: UILabel = {
        let label = UILabel(text: "", font: .systemFont(ofSize: 14), numberOfLines: 0)
        label.numberOfLines = 0
        label.minimumScaleFactor = 0.5
        return label
    }()
    
    let readingScoreLabel: UILabel = {
        let label = UILabel(text: "", font: .systemFont(ofSize: 14), numberOfLines: 0)
        label.numberOfLines = 0
        label.minimumScaleFactor = 0.5
        return label
    }()
    
    let writingScoreLabel: UILabel = {
        let label = UILabel(text: "", font: .systemFont(ofSize: 14), numberOfLines: 0)
        label.numberOfLines = 0
        label.minimumScaleFactor = 0.5
        return label
    }()
    
    let scoresTitleLabel: UILabel = {
        let label = UILabel(text: "SAT Scores", font: .boldSystemFont(ofSize: 20))
        return label
    }()
    
    let viewTrailerButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Website", for: .normal)
        button.backgroundColor = #colorLiteral(red: 0, green: 0.4784313725, blue: 0.937254902, alpha: 1)
        button.layer.cornerRadius = 32 / 2
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16)
        button.setTitleColor(.white, for: .normal)
        button.constrainWidth(constant: 125)
        return button
    }()
    
    let viewOnMapButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("View Map", for: .normal)
        button.backgroundColor = #colorLiteral(red: 0, green: 0.4784313725, blue: 0.937254902, alpha: 1)
        button.layer.cornerRadius = 32 / 2
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16)
        button.setTitleColor(.white, for: .normal)
        button.constrainWidth(constant: 125)
        return button
    }()
    
    let favoriteButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Favorite", for: .normal)
        button.setTitleColor(.blue, for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16)
        button.backgroundColor = UIColor(white: 0.95, alpha: 1)
        button.layer.cornerRadius = 32 / 2
        button.constrainHeight(constant: 32)
        button.constrainWidth(constant: 125)
        
        return button
    }()
    
    let descriptionTitleLabel = UILabel(text: "Description", font: .boldSystemFont(ofSize: 20))
    let longDescriptionLabel = UILabel(text: "", font: .systemFont(ofSize: 18), numberOfLines: 0)
    
    override func prepareForReuse() {
        logoImageView.image = nil
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        viewTrailerButton.addTarget(self, action: #selector(viewTrailerPressed(_:)), for: .touchUpInside)
        viewOnMapButton.addTarget(self, action: #selector(viewMapPressed(_:)), for: .touchUpInside)
        favoriteButton.addTarget(self, action: #selector(favoriteButtonPressed(_:)), for: .touchUpInside)

        let stackView = VerticalStackView(arrangedSubviews: [
            UIStackView(arrangedSubviews: [
                logoImageView,
                VerticalStackView(arrangedSubviews: [
                    UIView(),
                    UIStackView(arrangedSubviews: [UIView(), viewTrailerButton, UIView()]),
                    UIStackView(arrangedSubviews: [UIView(), viewOnMapButton, UIView()]),
                    UIStackView(arrangedSubviews: [UIView(), favoriteButton, UIView()]),
                    UIView()
                    ], spacing: 12)
                ], customSpacing: 20),
            scoresTitleLabel,
            mathScoreLabel,
            readingScoreLabel,
            writingScoreLabel,
            descriptionTitleLabel,
            longDescriptionLabel
            ], spacing: 5)
        addSubview(stackView)
        stackView.fillSuperview(padding: .init(top: 20, left: 20, bottom: 20, right: 20))
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError()
    }
    
    @objc func viewTrailerPressed(_ sender: UIButton){
        if let website = school?.website {
            guard let url = URL(string: "https://\(website)") else { return }
            UIApplication.shared.open(url)
        } else {
            print("No Preview")
        }
    }
    
    @objc func viewMapPressed(_ sender: UIButton){
        viewMapActionPressed?()
    }
    
    
    @objc func favoriteButtonPressed(_ sender: UIButton) {
        guard let school = school else { return }
        
        if !favorited {
            setToFavorite()
            persistSchoolHandler?(school)
        } else {
            resetFavoriteButton()
            removeFavoriteHandler?(school)
        }
    }
    
    func setToFavorite() {
        UIView.animate(withDuration: 0.25) { [weak self] in
            self?.favoriteButton.backgroundColor = .yellow
            self?.favoriteButton.setImage(UIImage(named: "favorites"), for: .normal)
            self?.favoriteButton.setTitle("", for: .normal)
            self?.favoriteButton.imageEdgeInsets = UIEdgeInsets(top: 3, left: 3, bottom: 3, right: 3)
        }
    }
    
    func resetFavoriteButton() {
        UIView.animate(withDuration: 0.25) { [weak self] in
            self?.favoriteButton.setTitle("FAVORITE", for: .normal)
            self?.favoriteButton.backgroundColor = UIColor(white: 0.95, alpha: 1)
            self?.favoriteButton.setImage(nil, for: .normal)
        }
    }
}
