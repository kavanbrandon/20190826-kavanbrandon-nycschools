//
//  ActivityCell.swift
//  20190826-KavanBrandon-NYCSchools
//
//  Created by Kavan Brandon on 8/27/19.
//  Copyright © 2019 KavanBrandon. All rights reserved.
//

import UIKit

class ActivityCell: UICollectionViewCell {
    
    let activityLabel = UILabel(text: "", font: .boldSystemFont(ofSize: 14), numberOfLines: 0)
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = #colorLiteral(red: 0.9423103929, green: 0.9410001636, blue: 0.9745038152, alpha: 1)
        layer.cornerRadius = 16
        clipsToBounds = true
        
        addSubview(activityLabel)
        activityLabel.centerInSuperview()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError()
    }
}

