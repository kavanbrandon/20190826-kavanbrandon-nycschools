//
//  ActivitiesRowCell.swift
//  20190826-KavanBrandon-NYCSchools
//
//  Created by Kavan Brandon on 8/27/19.
//  Copyright © 2019 KavanBrandon. All rights reserved.
//

import UIKit

class ActivitiesRowCell: UICollectionViewCell {
    
    let activitesTitleLabel = UILabel(text: "Extracurricular Activities", font: .boldSystemFont(ofSize: 20))
    let activitiesController = ActivitesController()
    
    override init(frame: CGRect) {
        super.init(frame: frame)

        addSubview(activitesTitleLabel)
        addSubview(activitiesController.view)
        
        activitesTitleLabel.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor, padding: .init(top: 0, left: 20, bottom: 0, right: 20))
        
        activitiesController.view.anchor(top: activitesTitleLabel.bottomAnchor, leading: leadingAnchor, bottom: bottomAnchor, trailing: trailingAnchor, padding: .init(top: 20, left: 0, bottom: 0, right: 0))
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError()
    }
}
