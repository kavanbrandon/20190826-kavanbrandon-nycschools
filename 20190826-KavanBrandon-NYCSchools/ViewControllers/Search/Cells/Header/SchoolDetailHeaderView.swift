//
//  SchoolDetailHeaderView.swift
//  20190826-KavanBrandon-NYCSchools
//
//  Created by Kavan Brandon on 8/28/19.
//  Copyright © 2019 KavanBrandon. All rights reserved.
//

import UIKit

//This class is used to showcase a stretchy and blurred header on the SchoolDetailController.
class SchoolDetaileHeaderView: UICollectionReusableView {
    
    let headerImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "doe_classroom")
        imageView.contentMode = .scaleAspectFill
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    let titleLabel: UILabel = {
        let label = UILabel()
        label.text = "School Title Label"
        label.font = .systemFont(ofSize: 20, weight: .heavy)
        label.textColor = .white
        label.numberOfLines = 0
        return label
    }()
    
    let descriptionLabel: UILabel = {
        let label = UILabel()
        label.text = "DescriptionLabel"
        label.font = .systemFont(ofSize: 12, weight: .regular)
        label.textColor = .white
        label.numberOfLines = 0
        return label
    }()
    
    var animator: UIViewPropertyAnimator!
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .white
        
        addSubview(headerImageView)
        headerImageView.fillSuperview()

        setupVisualEffectBlur()
        setupGradientLayer()
    }
    
    fileprivate func setupGradientLayer() {
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [UIColor.clear.cgColor, UIColor.black.cgColor]
        gradientLayer.locations = [0,1]
        
        let gradientContainerView = UIView()
        addSubview(gradientContainerView)
        gradientContainerView.anchor(top: nil, leading: leadingAnchor, bottom: bottomAnchor, trailing: trailingAnchor)
        gradientContainerView.layer.addSublayer(gradientLayer)
        gradientLayer.frame = self.bounds
        gradientLayer.frame.origin.y -= bounds.height
        
        let stackView = UIStackView(arrangedSubviews: [
            titleLabel, descriptionLabel
            ], customSpacing: 5)
        stackView.axis = .vertical
        
        addSubview(stackView)
        stackView.anchor(top: nil, leading: leadingAnchor, bottom: bottomAnchor, trailing: trailingAnchor, padding: .init(top: 0, left: 16, bottom: 16, right: 16))
    }
    
    fileprivate func setupVisualEffectBlur() {
        animator = UIViewPropertyAnimator(duration: 3.0, curve: .linear, animations: { [weak self] in
            let blurEffect = UIBlurEffect(style: .regular)
            let visualEffectView = UIVisualEffectView(effect: blurEffect)
            
            self?.addSubview(visualEffectView)
            visualEffectView.fillSuperview()
        })
    }
}

