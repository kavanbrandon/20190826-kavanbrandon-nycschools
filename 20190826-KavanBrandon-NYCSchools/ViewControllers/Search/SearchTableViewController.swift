//
//  SearchTableViewController.swift
//  20190826-KavanBrandon-NYCSchools
//
//  Created by Kavan Brandon on 8/26/19.
//  Copyright © 2019 KavanBrandon. All rights reserved.
//

import UIKit

class SearchTableViewController: UITableViewController, UISearchBarDelegate {
    
    var schools = [School]()
    fileprivate var cache = Dictionary<String, [School]>()
    fileprivate let schoolResultCellID = "schoolResultCellID"
    
    var continueFetching = false
    var stopFetching = false
    var searching = false
    var searchTerm = ""
    
    let searchController = UISearchController(searchResultsController: nil)
    var timer: Timer?
    
    fileprivate let noResultsLabel: UILabel = {
        let label = UILabel()
        label.text = "No Results"
        label.textAlignment = .center
        label.font = UIFont.boldSystemFont(ofSize: 20)
        return label
    }()

    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.addSubview(noResultsLabel)
        noResultsLabel.translatesAutoresizingMaskIntoConstraints = false
        noResultsLabel.centerXInSuperview()
        noResultsLabel.anchor(top: tableView.topAnchor, leading: nil, bottom: nil, trailing: nil, padding: UIEdgeInsets(top: 150, left: 0, bottom: 16, right: 0))
        
        setupSearchBar()
        tableView.register(SubtitleTableViewCell.self, forCellReuseIdentifier: schoolResultCellID)
        
        fetchSchoolList()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        cache.removeAll()
        searchController.searchBar.text = ""
        stopFetching = false
    }
    
    fileprivate func setupSearchBar() {
        definesPresentationContext = true
        navigationItem.searchController = self.searchController
        navigationItem.hidesSearchBarWhenScrolling = false
        searchController.dimsBackgroundDuringPresentation = false
        searchController.searchBar.delegate = self
        searchController.searchBar.placeholder = "School Name"
    }
    
    func fetchSchoolList() {
        let offset = 0
        if let schools = cache["\(offset)"] {
            self.schools += schools
            self.tableView.reloadData()
            return
        } else {
            APIService.shared.fetchSchools(offset: offset) { [weak self] (schools, error) in
                self?.cache["fetching + \(offset)"] = schools
                self?.schools = schools
                DispatchQueue.main.async {
                    self?.tableView.reloadData()
                }
            }
        }
    }
    
    func searchSchools(searchTerm: String, offset: Int) {
        let updatedSearchTerm = searchTerm.replace(target: " ", withString: "%25")
        
        //A timer is used to fire off period API requests when searching.
        timer?.invalidate()
        timer = Timer.scheduledTimer(withTimeInterval: 0.5, repeats: false, block: { [weak self] (_) in
            if let schools = self?.cache["\(updatedSearchTerm)+\(self?.schools.count ?? 0)"] {
                self?.schools = schools
                self?.tableView.reloadData()
                return
            } else {
                APIService.shared.searchSchools(searchTerm: updatedSearchTerm, offset: 0) { (schools, error) in
                    self?.cache["\(updatedSearchTerm)+\(self?.schools.count ?? 0)"] = schools
                    self?.schools = schools
                    DispatchQueue.main.async {
                        self?.tableView.reloadData()
                    }
                }
            }
        })
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        resetSearch()
        if searchText != "" {
            searching = true
            searchTerm = searchText
            let updatedSearchTerm = searchText.replace(target: " ", withString: "%25")
            searchSchools(searchTerm: updatedSearchTerm, offset: 0)
        } else {
            fetchSchoolList()
        }
    }
    
    func resetSearch() {
        timer?.invalidate()
        searching = false
        searchTerm = ""
        stopFetching = false
        schools.removeAll()
        tableView.reloadData()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        resetSearch()
        fetchSchoolList()
    }
    
    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        noResultsLabel.isHidden = schools.count > 0
        tableView.separatorStyle = schools.count > 0 ? .singleLine : .none
        return schools.count
    }
    
    //The tableview will update depending on if a user is entering a search criteria or not. If there is no searching, the tableview will showcase the list of schools. Pagination is also embedded in both scenarios.
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: schoolResultCellID, for: indexPath)
        let school = schools[indexPath.row]
        cell.textLabel?.text = school.schoolName
        cell.textLabel?.adjustsFontSizeToFitWidth = true
        let editedBoroughString = school.borough?.lowercased().capitalizingFirstLetter()
        cell.detailTextLabel?.text = "\(school.neighborhood ?? ""), \(editedBoroughString ?? "")"
        cell.accessoryType = .disclosureIndicator

        if !searching {
            // With more time, I would have check to see if the request is available in the cache before executing the below code.
            if indexPath.item == schools.count - 1 && !stopFetching {
                continueFetching = true
                APIService.shared.fetchSchools(offset: schools.count) { [weak self] (schools, error) in
                    if schools.count == 0 {
                        self?.stopFetching = true
                    } else {
                        // With more time, I would save the results of the request to the cache in order to prevent uneccessary requests for data we've received in the past.
                        //self?.cache["fetching + \(self?.schools.count ?? 0)"] = schools
                    }
                    
                    //Using this line of code to demonstrate the pagination feature. In a production environment I would not include this.
                    sleep(2)
                    
                    self?.schools += schools
                    DispatchQueue.main.async {
                        self?.tableView.reloadData()
                    }
                    self?.continueFetching = false
                }
            }
        } else {
            let updatedSearchTerm = searchTerm.replace(target: " ", withString: "%25")
            
            // With more time, I would have check to see if the request is available in the cache before executing the below code.
            if indexPath.item == schools.count - 1 && !stopFetching {
                continueFetching = true
                APIService.shared.searchSchools(searchTerm: updatedSearchTerm, offset: schools.count) { [weak self] (schools, error) in
                    if schools.count == 0 {
                        self?.stopFetching = true
                    } else {
                        // With more time, I would save the results of the request to the cache in order to prevent uneccessary requests for data we've received in the past.
                        //self?.cache["\(updatedSearchTerm)+\(self?.schools.count ?? 0)"] = schools
                    }
                    
                    //Using this line of code to demonstrate the pagination feature. In a production environment I would not include this.
                    sleep(2)
                    
                    self?.schools += schools
                    DispatchQueue.main.async {
                        self?.tableView.reloadData()
                    }
                    self?.continueFetching = false
                }
            }
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let dbn = schools[indexPath.row].dbn {
            let controller = SchoolDetailController(dbn: dbn)
            controller.navigationItem.title = schools[indexPath.row].schoolName
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }

    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastSectionIndex = tableView.numberOfSections - 1
        let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
        if indexPath.section ==  lastSectionIndex && indexPath.row == lastRowIndex {
            let spinner = UIActivityIndicatorView(style: .gray)
            spinner.startAnimating()
            spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))
            tableView.tableFooterView = spinner
            tableView.tableFooterView?.isHidden = stopFetching
        }
    }
}
