//
//  MapViewController.swift
//  20190826-KavanBrandon-NYCSchools
//
//  Created by Kavan Brandon on 8/27/19.
//  Copyright © 2019 KavanBrandon. All rights reserved.
//

import UIKit
import MapKit

class MapViewController: UIViewController, MKMapViewDelegate {
    
    let mapView = MKMapView()
    var school: School?
    
    convenience init(school: School) {
        self.init()
        self.school = school
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
                
        mapView.mapType = MKMapType.standard
        mapView.isZoomEnabled = true
        mapView.isScrollEnabled = true
        mapView.center = view.center
        view.addSubview(mapView)
        mapView.fillSuperview()
        
        let schoolAnnotation = MKPointAnnotation()
        schoolAnnotation.title = school?.schoolName
        
        if let latitude = school?.latitude, let longitude = school?.longitude {
            schoolAnnotation.coordinate = CLLocationCoordinate2D(latitude: Double(latitude)!, longitude: Double(longitude)!)
        }
        mapView.addAnnotation(schoolAnnotation)
        mapView.showAnnotations(mapView.annotations, animated: true)
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        guard annotation is MKPointAnnotation else { return nil }
        
        let identifier = "Annotation"
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier)
        
        if annotationView == nil {
            annotationView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: identifier)
            annotationView!.canShowCallout = true
        } else {
            annotationView!.annotation = annotation
        }
        
        return annotationView
    }
}
