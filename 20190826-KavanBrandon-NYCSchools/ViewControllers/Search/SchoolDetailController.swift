//
//  SchoolDetailController.swift
//  20190826-KavanBrandon-NYCSchools
//
//  Created by Kavan Brandon on 8/27/19.
//  Copyright © 2019 KavanBrandon. All rights reserved.
//

import UIKit

@objc class SchoolDetailController: BaseCollectionViewController, UICollectionViewDelegateFlowLayout {
    
    @objc var dbn: String = String()
    var school: School?
    var result: Result?
    var activities = [String]()
    var headerView: SchoolDetaileHeaderView?
    private var backgroundGroup = DispatchGroup()
    private var backgroundQueue = DispatchQueue(label: "com.nyc.schools.bgqueue")
    fileprivate var coreDataManager: CoreDataManager!
    
    fileprivate let schoolDetailCellID = "schoolDetailCellID"
    fileprivate let activityRowCellID = "activityRowCellID"
    fileprivate let headerViewCellID = "headerViewCellID"
    
    @objc init(dbn: String) {
        self.dbn = dbn
        super.init()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.backgroundColor = .white
        
        collectionView.register(SchoolDetailCell.self, forCellWithReuseIdentifier: schoolDetailCellID)
        collectionView.register(ActivitiesRowCell.self, forCellWithReuseIdentifier: activityRowCellID)
        collectionView.register(SchoolDetaileHeaderView.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: headerViewCellID)
        edgesForExtendedLayout = [.bottom]
        navigationController?.navigationBar.isTranslucent = false

        navigationItem.largeTitleDisplayMode = .always
        fetchSchoolData()
        
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        coreDataManager = CoreDataManager(container: appDelegate.persistentContainer)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        headerView?.animator.stopAnimation(true)
    }
    
    fileprivate func fetchSchoolData() {
        
        self.backgroundGroup.enter()
        self.backgroundQueue.async { [weak self] in
            
            guard let dbn = self?.dbn else { return }

            APIService.shared.fetchSchool(dbn: dbn) { [weak self] (result: [School], error) in
                if let error = error {
                    print(error)
                    self?.backgroundGroup.leave()
                    return
                }

                self?.school = result.first
                self?.backgroundGroup.leave()
            }
        }

        self.backgroundGroup.enter()
        self.backgroundQueue.async { [weak self] in
            
            guard let dbn = self?.dbn else { return }

            APIService.shared.fetchSATResult(dbn: dbn) { [weak self] (result: [Result], error) in
                if let error = error {
                    print(error)
                    self?.backgroundGroup.leave()
                    return
                }

                self?.result = result.first
                self?.backgroundGroup.leave()
            }

        }

        self.backgroundGroup.notify(queue: DispatchQueue.main) { [weak self] in
            self?.collectionView.reloadData()
        }
    }
    
    //This method is used to apply the blur and stretch effect on the header view depending on the scroll position.
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let contentOffsetY = scrollView.contentOffset.y
        
        if contentOffsetY > 0 {
            headerView?.animator.fractionComplete = 0
            return
        }
        headerView?.animator.fractionComplete = abs(contentOffsetY) / 100
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return .init(width: view.frame.width, height: 300)
    }
    
    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: headerViewCellID, for: indexPath) as? SchoolDetaileHeaderView
        if let school = school {
            headerView?.titleLabel.text = school.schoolName
            headerView?.descriptionLabel.text = "\(school.primaryAddressLine1 ?? ""), \(school.city ?? ""), NY, \(school.zip ?? "")"
        }
        return headerView!
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 2
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.item == 0 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: schoolDetailCellID, for: indexPath) as! SchoolDetailCell
            if let school = school, let result = result, let dbn = school.dbn {
                cell.school = school
                cell.result = result
                cell.viewMapActionPressed = { [weak self] in
                    let mapViewController = MapViewController(school: school)
                    mapViewController.navigationItem.title = school.schoolName
                    self?.navigationController?.pushViewController(mapViewController, animated: true)
                }
                
                if checkIfFavoriteExists(dbn: dbn) {
                    cell.setToFavorite()
                    cell.favorited = true
                }
                
                cell.persistSchoolHandler = { [weak self] school in
                    if !((self?.checkIfFavoriteExists(dbn: dbn))!) {
                        self?.persistSchoolToFavorites(school: school)
                        cell.favorited = true
                    }
                }
                
                cell.removeFavoriteHandler = { [weak self] school in
                    if (self?.checkIfFavoriteExists(dbn: dbn))! {
                        self?.removeSchoolFromFavorites(dbn: dbn)
                        cell.favorited = false
                    }
                }
            }
            return cell
        } else {
            //This cell is designed to showcase having a horizontal collectionview within a vertical collectionview
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: activityRowCellID, for: indexPath) as! ActivitiesRowCell
            cell.activitiesController.school = school
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        var height: CGFloat = 280
        
        if indexPath.item == 0 {
            // calculate resizing cell
            let resizingCell = SchoolDetailCell(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: 1000))
            resizingCell.school = school
            resizingCell.result = result
            resizingCell.layoutIfNeeded()
            
            let estimatedSize = resizingCell.systemLayoutSizeFitting(CGSize(width: view.frame.width, height: 1000))
            height = estimatedSize.height
        } else {
            height = 200
        }
        
        return CGSize(width: view.frame.width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 16, right: 0)
    }
    
    
    func checkIfFavoriteExists(dbn: String) -> Bool {
        return coreDataManager.checkIfFavoriteExists(dbn: dbn)
    }
    
    func persistSchoolToFavorites(school: School) {
        coreDataManager.persistSchoolToFavorites(school: school)
    }
    
    func removeSchoolFromFavorites(dbn: String) {
        coreDataManager.removeSchoolFromFavorites(dbn: dbn)
    }
}
