//
//  FavoritesTableViewController.m
//  20190826-KavanBrandon-NYCSchools
//
//  Created by Kavan Brandon on 8/28/19.
//  Copyright © 2019 KavanBrandon. All rights reserved.
//

#import "FavoritesTableViewController.h"
#import "_0190826_KavanBrandon_NYCSchools-Swift.h"

@interface FavoritesTableViewController ()

@property (nonatomic, strong) NSArray *favorites;

@end

//This file is used for the purposes of showcasing using an Objective-C file in a Swift project.
//The controller accesses core data to retreive any favorited schools.
@implementation FavoritesTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = appDelegate.persistentContainer.viewContext;
    NSFetchRequest *fetch = [[NSFetchRequest alloc] init];
    NSEntityDescription *entityDescription = [NSEntityDescription entityForName:@"School"  inManagedObjectContext: context];
    [fetch setEntity:entityDescription];
    NSError *error = nil;
    self.favorites = [context executeFetchRequest:fetch error:&error];
    [self.tableView reloadData];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.favorites.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"schoolCellID";
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
    cell.textLabel.adjustsFontSizeToFitWidth = YES;
    
    //With more time, I would have considered a helper method to convert the NSManagedObjectContext back into a School object.
    //This would assist with not having to access the properties via valueForKey
    NSManagedObject *school = self.favorites[indexPath.row];
    NSString *borough = [school valueForKey:@"borough"];
    NSString *editedBoroughString = [NSString stringWithFormat:@"%@%@",[[borough substringToIndex:1] uppercaseString],[[borough substringFromIndex:1] lowercaseString]];
    
    cell.textLabel.text = [school valueForKey:@"schoolName"];
    cell.detailTextLabel.text = [NSString stringWithFormat:@"%@, %@",[school valueForKey:@"neighborhood"], editedBoroughString];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *dbn = [self.favorites[indexPath.row] valueForKey:@"dbn"];
    
    //Purposefully showcasing accessing a Swift class from an Objective-C file.
    SchoolDetailController *controller = [[SchoolDetailController alloc] initWithDbn: dbn];
    controller.title = [self.favorites[indexPath.row] valueForKey:@"schoolName"];
    [self.navigationController pushViewController:controller animated:YES];
}

@end
