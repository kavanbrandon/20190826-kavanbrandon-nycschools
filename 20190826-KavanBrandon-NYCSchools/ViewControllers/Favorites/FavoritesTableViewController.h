//
//  FavoritesTableViewController.h
//  20190826-KavanBrandon-NYCSchools
//
//  Created by Kavan Brandon on 8/28/19.
//  Copyright © 2019 KavanBrandon. All rights reserved.
//

#import <UIKit/UIKit.h>

@class AppDelegate;
@class SchoolDetailController;

@interface FavoritesTableViewController : UITableViewController

@end
