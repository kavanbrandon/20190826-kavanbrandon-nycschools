#### Features
1. Search NYC Schools
2. List NYC Schools
3. View school name, description, SAT scores, website
4. Identify school location on MKMap View
5. Persist favorite schools using Core Data
6. Pagination when searching or listing schools
7. School detail header view stretches and blurs depending on scroll position
8. Bridging header to access Obj-C view controller from Swift
9. Unit test examples

#### Models
1. School
2. Result

