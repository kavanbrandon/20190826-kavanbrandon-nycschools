//
//  _0190826_KavanBrandon_NYCSchoolsTests.swift
//  20190826-KavanBrandon-NYCSchoolsTests
//
//  Created by Kavan Brandon on 8/26/19.
//  Copyright © 2019 KavanBrandon. All rights reserved.
//

import XCTest
@testable import _0190826_KavanBrandon_NYCSchools

class _0190826_KavanBrandon_NYCSchoolsTests: XCTestCase {
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.

    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    //With more time, this section wuold test view controller initialization to ensure properties are accessibable.
    func testSchoolDetailControllerDBN() {
        let dbn = "29Q283"
        let controller = SchoolDetailController(dbn: dbn)
        XCTAssertEqual(dbn, controller.dbn)
    }
    
    func testMapViewControllerLatLong() {
        let school = School(dbn: "1234", schoolName: "Bronx School", overviewParagraph: "Amazing School", neighborhood: "University Heights", location: "Bronx", phoneNumber: "888-8888", schoolEmail: "school@test.com", website: "www.school.com", subway: "A", bus: "M19", totalStudents: "125", extracurricularActivities: "Robotics", schoolSports: "Basketball", attendanceRate: ".5", directions1: "Go Right", primaryAddressLine1: "201 West 34th Street", city: "Bronx", zip: "10453", latitude: "40.736526", longitude: "-73.992727", borough: "Bronx")

        let controller = MapViewController(school: school)
        
        if let latitude = controller.school?.latitude, let longitude = controller.school?.longitude {
            XCTAssertEqual(school.latitude, latitude)
            XCTAssertEqual(school.longitude, longitude)
        }
    }
    
    //With more time, this section would be used to test extensions, helper methods, or any other business logic.
    func testReplaceEmptySpacesWithCharactersForURLStrings() {
        let school = "Bronx Science"
        let updatedString = school.replace(target: " ", withString: "%25")
        XCTAssertEqual(updatedString, "Bronx%25Science")
    }
    
    func testCapitalizingCityName() {
        let city = "bronx"
        let capitalizedFirstLetterCity = city.capitalizingFirstLetter()
        XCTAssertEqual(capitalizedFirstLetterCity, "Bronx")
    }
}
